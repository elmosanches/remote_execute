# from __future__ import with_statement
import re
from fabric.api import *
from fabric.contrib.console import confirm

import argparse
import ConfigParser


"""
Executing command on a list of hosts

usage:
    To use the tool change to directory containing the fabfile.py

    fab --hosts=<host1,host2,...> remote_execute:command=<command to execute>
    fab -c <path-to-config_file> remote_execute

Config file requires options in form: <option> = <value>
line starting with # is a comment
Available options are:
    hosts = <user@host1[, user@host2, ...]> (requeired)
        comma seperated list of target hosts

    command = <command-to-execute> (requeired)
        command that will be executed on target host

    target_dir = <path>
        taret directory in which command executing will be started
        default value: /

    seperator = <pattern>
        line that will be printed after executin on a host has finished
        default value: --------------------------

    shell = <command>
        command that will be executed as shell
        default value: /bin/bash -c
"""


DEFAULT_WORKING_DIRECTORY = '/'
DEFAULT_SEPARATOR = '--------------------------'

if env.hosts and not isinstance(env.host, list):
    re.sub("\s+" , "", env.hosts)
    try:
        env.hosts = env.hosts.split(',')
    except:
        print 'hosts in config file can\'t be parsed'
        exit(1)

if not getattr(env, 'seperator', None):
    env.seperator = DEFAULT_SEPARATOR

conf_command = env.command
conf_target_dir = env.target_dir

def remote_execute(command=conf_command, target_dir=conf_target_dir):
    with cd(target_dir), hide('running', 'warnings'):
        run(command, warn_only=True).succeeded
        print env.seperator
